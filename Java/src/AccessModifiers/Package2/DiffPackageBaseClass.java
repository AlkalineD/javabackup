package AccessModifiers.Package2;


import AccessModifiers.BasePackage.BaseClass;

public class DiffPackageBaseClass extends BaseClass {
    public static void main(String[] args) {
        DiffPackageBaseClass obj = new DiffPackageBaseClass();
        System.out.println(obj.publicField);
        System.out.println(obj.protectedField);
    }
}