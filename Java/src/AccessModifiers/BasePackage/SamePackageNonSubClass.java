package AccessModifiers.BasePackage;


public class SamePackageNonSubClass {
    public static void main(String[] args) {
        BaseClass obj = new BaseClass();
        System.out.println(obj.publicField);
        System.out.println(obj.protectedField);
        System.out.println(obj.defaultField);

    }
}
