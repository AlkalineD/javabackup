package AccessModifiers.BasePackage;

public class SamePackageSubClass extends BaseClass {
    public static void main(String[] args) {
        SamePackageSubClass obj = new SamePackageSubClass();
        System.out.println(obj.defaultField);
        System.out.println(obj.protectedField);
        System.out.println(obj.publicField);
    }
}