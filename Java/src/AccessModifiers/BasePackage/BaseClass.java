package AccessModifiers.BasePackage;

public class BaseClass {
    public String publicField = "Public";
    private String privateField = "Private";
    protected String protectedField = "Protected";
    String defaultField = "Default";
}