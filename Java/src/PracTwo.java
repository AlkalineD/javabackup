class MethodClass {

    private int id;
    private String name;


    public String getName() {
        return name;
    }


    public int getId() {
        return id;
    }


    public void setName(String name) {
        this.name = name;
    }


    public void setId(int id) {
        this.id = id;
    }

}


public class PracTwo {
    public static void main(String[] args) {
        MethodClass obj = new MethodClass();
        obj.setId(69);
        obj.setName("Wakanda Forever");
        System.out.println();
        System.out.println(obj.getId());
        System.out.println(obj.getName());
    }
}
