import java.util.ArrayList;
import java.util.Scanner;

public class RandomFile {
    static void longestIncreasingSeq(int n, int[] arr){
        ArrayList<Integer> arrayList = new ArrayList<>();
        int count = 1;
        for(int i = 1; i < n; ++i){
            int prev = arr[i - 1];
            if(arr[i] > prev){
                ++count;
            }

            if(prev > arr[i]){
                arrayList.add(count);
                count = 0;
            }
        }
        System.out.println(arrayList);
    }



    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
//        int n = Integer.parseInt(scanner.nextLine());
        longestIncreasingSeq(9, new int[] {11, 3, 7, 8, 12, 2, 3, 7, 0});

        
    }
}
