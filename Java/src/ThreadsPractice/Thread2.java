package ThreadsPractice;

class SalesPersons extends Thread {
    String[] salesPerson = {"One", "Two", "Three", "Four", "Five"};    // five salespersons names

    public void run() {
        for (String name : salesPerson) {
            System.out.println(name);
        }
    }
}

class Days extends Thread {
    String[] days = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

    public void run() {
        SalesPersons salesPersons = new SalesPersons();
        salesPersons.run();

        for (String day : days) {
            System.out.println(day);

            if (day.equals("Wednesday")) {
                System.out.println("Resuming Sales");
                salesPersons.resume();
            }

            if(day.equals("Sunday")) {
                System.out.println("Suspending Sales");
                salesPersons.suspend();
            }

        }
    }

}

public class Thread2 {
    public static void main(String[] args) {

        Days days = new Days();
        days.run();

    }
}

