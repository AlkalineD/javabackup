package ThreadsPractice;

    class Scooby extends Thread{

        public void run(){
            System.out.println("----Scooby Thread----");
        }
    }

    class Shaggy extends Thread{

        public void run(){
            System.out.println("----Shaggy Thread----");
        }
    }



    public class Thread1{
        public static void main(String[] args) {
            Scooby scooby = new Scooby();
            scooby.run();
            scooby.setName("Scooby");
            System.out.println(scooby.getName());

            Shaggy shaggy = new Shaggy();
            shaggy.run();
            shaggy.setName("Shaggy");
            System.out.println(shaggy.getName());

        }
}
