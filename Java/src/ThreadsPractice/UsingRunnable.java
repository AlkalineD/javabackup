package ThreadsPractice;

class ThisHasThread implements Runnable{

    @Override
    public void run() {
        for(int i = 0; i<5; ++i){
            System.out.println("1");
        }
    }
}

public class UsingRunnable {
    public static void main(String[] args) {
        ThisHasThread thisHasThread = new ThisHasThread();
        Thread thread = new Thread(thisHasThread);


    }
}
