import java.sql.*;

public class Jdbc {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/testing?autoReconnect=true&useSSL=false";

    public static void main(String[] args){
        try{
            Class.forName(JDBC_DRIVER);

            System.out.println("Connecting to database...");
            Connection conn = DriverManager.getConnection(DB_URL, "test_user", "Hello@234");

            Statement stmt = conn.createStatement();

            //System.out.println(stmt.executeUpdate("insert into db values (49, 'sarthak')"));
            ResultSet rs = stmt.executeQuery("select name from db where id = 48");


            while(rs.next()){
                System.out.println(" " + rs.getString(1));
            }

            conn.close();
            stmt.close();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }


    }
}
