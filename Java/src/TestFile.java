import java.util.*;

public class TestFile {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add("N");
        set.add("P");
        set.add("T");
        set.add("E");
        set.add("L");

        set.stream().forEach(System.out::println);

    }
}
