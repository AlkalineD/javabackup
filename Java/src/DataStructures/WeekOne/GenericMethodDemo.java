package DataStructures.WeekOne;

public class GenericMethodDemo{
    <T> void genericMehod(T t){

        System.out.println(t);
    }

    static <T> void genericMethod2(T t){
        System.out.println(t);
    }

    <T> void genericMethod(T x, T y){
        System.out.println(x);
        System.out.println(y);
    }

    <T> void swap(T x, T y){
        T temp;
        temp = x;
        x = y;
        y = temp;

        System.out.println("In Method:" + x +", "+ y);

    }

    public static  void main(String... args){

        GenericMethodDemo object = new GenericMethodDemo();
        object.genericMehod("Hello");
        GenericMethodDemo.genericMethod2(344);
        object.genericMethod("Hello", 78);

        double x = 4.4;
        int y = 2;
        object.swap(x, y);
        System.out.println("In Main: "+x +", "+ y);






    }
}
