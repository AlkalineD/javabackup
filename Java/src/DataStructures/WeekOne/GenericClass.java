package DataStructures.WeekOne;

class ThisIsGenericClass <T>{
    private T x;
    private T y;

    ThisIsGenericClass(T x){
        this.x = x;
    }

    void setFields(T x, T y){
        this.x = x;
        this.y = y;
    }

    void print(){
        System.out.println(x);
        System.out.println(y);
    }

    void print2(){
        System.out.println(x);
    }

    void methodTest(int x){
        System.out.println(x);
    }
}

class cls2 extends ThisIsGenericClass{

    cls2(Object x) {
        super(x);
    }
}

public class GenericClass{
    public static void main(String[] args){
        ThisIsGenericClass obj = new ThisIsGenericClass(2);
        obj.setFields(2, "String");
        obj.print();
        obj.setFields(7.7, "String");
        obj.print();

        ThisIsGenericClass<String> a = new ThisIsGenericClass<String>("Java");
        ThisIsGenericClass<Integer> b = new ThisIsGenericClass<Integer>(22);
        a.print2();
        b.print2();
        obj.methodTest(69);

    }
}
