package DataStructures.WeekThree;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

// all methods are static

public class ArrayClass {
    public static void main(String[] args){
        int[] arr = {1, 2, 3 , 5, 7, 69};
        Integer[] ints = new Integer[] {1, 2, 3, 5, 7, 69};
//        List obj = Arrays.asList(arr);
//        System.out.println(obj);   // returns some wierd string
        List ob = Arrays.asList(ints);
        System.out.println(ob);                                     // on using Integer wrapper class...it returns similar list  There's no such thing as a List<int> in Java - generics don't support primitives. Autoboxing only happens for a single element, not for arrays of primitives.

        String[] str = { "Geeks", "for", "Geeks" };

        // Using Arrays.stream() to convert
        // array into Stream
        Stream<Integer> stream = Arrays.stream(ints);
        stream.forEach(x-> System.out.println(x));                 // use labda expressions
    }
}


