package DataStructures.WeekThree;
import java.util.Vector;
public class VectorArray {
    public static void main(String[] args){
        Vector vector = new Vector(3, 2);
        vector.add(1);
        vector.add(2);
        vector.add(3);
        System.out.println(vector);
        vector.remove(2);
        System.out.println(vector);
        vector.add(1,"Hello");
        System.out.println(vector.capacity());
        vector.add(6.9);
        vector.addElement(new Float(4.20));
        System.out.println(vector);
        vector.add(2, "element");
        System.out.println(vector.capacity());
        System.out.println(vector.size());
        vector.remove(2);
        System.out.println(vector);

        System.out.println(vector.indexOf(1));
        Object x = vector.get(3);
        System.out.println(x);

    }
}
