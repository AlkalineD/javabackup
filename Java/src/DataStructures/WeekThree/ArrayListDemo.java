package DataStructures.WeekThree;

import java.util.ArrayList;

public class ArrayListDemo {
    public static void main(String[] args){
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(2);
        arrayList.add(23);
        arrayList.add(24);
        arrayList.add(25);
        arrayList.add(26);
        arrayList.add(2, 69);  //insert at index
        System.out.println(arrayList);
        arrayList.remove(4);            // remove at index
        System.out.println(arrayList);
        arrayList.remove(new Integer(23));      // remove some specific element
        System.out.println(arrayList);
        System.out.println(arrayList.size());
        arrayList.ensureCapacity(6);
        arrayList.trimToSize();
        System.out.println(arrayList.size());
    }
}
