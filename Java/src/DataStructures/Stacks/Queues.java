package DataStructures.Stacks;

import java.util.PriorityQueue;
import java.util.Queue;

public class Queues {
    public static void main(String[] args) {
        Queue queue = new PriorityQueue(){

        };
        queue.add(2);
        System.out.println(queue.peek());
        queue.offer("3");
        queue.clear();


        System.out.println(queue);

        Queue<Integer> q2 = new PriorityQueue<>();
        q2.add(23);
        q2.add(45);
        q2.add(1);
        System.out.println(q2.peek());
        q2.remove();
        System.out.println(q2.peek());
    }
}
