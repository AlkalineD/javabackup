package DataStructures.Stacks;

import java.util.Stack;

public class StacksDemo {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(Integer.valueOf("99"));
        stack.push(99);
        stack.push((int) 7.6);
        System.out.println(stack.peek())
        ;
    }
}
