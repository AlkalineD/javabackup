package Inheritance;

interface Interface1 {
    public void interfaceM();
}

interface Interface2 {
    public void interfaceM();
}

class Class3 implements Interface1, Interface2 {
    public void display() {
        System.out.println("Class3");

    }

    @Override
    public void interfaceM() {
        System.out.println("Hello");

    }
}


public class Multiple {
    public static void main(String[] args) {
        Class3 obj = new Class3();
        obj.display();
        obj.interfaceM();
        obj.interfaceM();
    }
}

