package Inheritance;

class StageOne {
    int field;

    public StageOne() {
        field = 45;
    }

    public void display() {
        System.out.println(field);
    }
}


class StageTwo extends StageOne {
    int data;

    public void setData(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }
}


class StageThree extends StageTwo {

    public void display2() {
        System.out.println("Stage 3");
    }
}


public class MultiLevel {
    public static void main(String[] args) {
        StageThree obj = new StageThree();
        obj.display();
        obj.setData(101);
        System.out.println(obj.getData());
    }
}

