package Inheritance;

class One {
    private int field;

    public One() {
        field = 0;
    }

    public void setField(int field) {
        this.field = field;
    }

    public int getField() {
        return this.field;
    }

}

class Two extends One {
    public void display() {
        System.out.println("Class");
    }
}


public class Single {
    public static void main(String[] args) {
        Two two = new Two();
        two.setField(404);
        System.out.println(two.getField());
        two.display();
    }
}


