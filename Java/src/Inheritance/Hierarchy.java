package Inheritance;

class First {
    public void firstDisplay() {
        System.out.println("Class: First");
    }
}

class Second extends First {
    public void secondDisplay() {
        System.out.println("Class: Second");
    }
}

class Third extends First {
    public void thirdDisplay() {
        System.out.println("Class: Third");
    }
}


public class Hierarchy {
    public static void main(String[] args) {
        Second second = new Second();
        Third third = new Third();
        second.firstDisplay();
        third.firstDisplay();
    }
}



