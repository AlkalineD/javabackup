package JavaIOPractice;

import java.io.*;
import java.util.Scanner;

public class ReadWriteFile {
    public static void main(String[] args) throws IOException {
        File obj = new File("Testfile.txt");
        if (obj.createNewFile()) {
            System.out.println("File created at " + obj.getAbsolutePath());
        } else {
            System.out.println("File already exists at " + obj.getCanonicalPath());
        }
        FileWriter ob = new FileWriter("TestFile.txt");// writing this to fileob.write("Writing to file");
        ob.write("\nFile ");
        ob.write("\n Done with writing");
        ob.close();
        System.out.println("\nReading FIle.... ");
        FileReader ob2 = new FileReader("Testfile.txt");
        try {
            File myObj = new File("testfile.txt");
            Scanner scanner = new Scanner(myObj);
            while (scanner.hasNextLine()) {
                String data = scanner.nextLine();
                System.out.println(data);
            }
            scanner.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

