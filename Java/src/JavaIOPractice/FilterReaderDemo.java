package JavaIOPractice;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class FilterReaderDemo {
    public static void main(String... args) throws IOException {
        Reader reader = new StringReader("Some long string");
        FilterReader filterReader = new FilterReader(reader) {

        };

        System.out.println(filterReader.ready());

        char[] array = new char[20];
        filterReader.skip(3);
        filterReader.read(array, 3, 6);
        for (char arr : array) {
            System.out.print(arr);
        }
        System.out.println();
        filterReader.skip(5);
        System.out.println((char) filterReader.read());

    }
}
