package JavaIOPractice;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOut {
    public static void main(String[] args) throws IOException {

        FileInputStream fileInputStream = new FileInputStream("TestFile.txt");
        byte[] bs = new byte[10];
        fileInputStream.skip(3);
        fileInputStream.read(bs);
        for(byte h: bs){
            System.out.println((char)h);
        }

        FileOutputStream fileOutputStream = new FileOutputStream("TestFile.txt");
        fileOutputStream.write('s');
        //fileInputStream.read(bs, 2, 4); ----> skips first two index in array bs and then writes at next 4
    }
}
